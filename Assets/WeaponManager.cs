using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum WeaponState
{
    Unarmed,
    Rifle,
    Pistol,
}

public class WeaponManager : MonoBehaviour
{
    public WeaponState state;

    [SerializeField]
    private Transform primaryGunModel;
    [SerializeField]
    private Transform pistolGunModel;

    private InputManager inputManager;
    private Animator animator;
    private PlayerController playerController;
    private CameraController cameraController;

    private void Start()
    {
        inputManager = FindObjectOfType<InputManager>();
        playerController = GetComponent<PlayerController>();
        cameraController = GetComponent<CameraController>();
        animator = playerController.GetAnimator();
    }

    private void Update()
    {
        PreProcessInput();

        ProcessWeaponInput();
    }

    private void PreProcessInput()
    {
        //If player press both primary and pistol then uncheck the pistol flag
        if (inputManager.primaryGun && inputManager.pistolGun)
            inputManager.pistolGun = false;
    }

    private void ProcessWeaponInput()
    {
        if (inputManager.primaryGun)
        {
            if (state == WeaponState.Unarmed)
            {
                state = WeaponState.Rifle;
                animator.SetBool("RifleEquiped", true);
                primaryGunModel.gameObject.SetActive(true);
            }
            else if (state == WeaponState.Pistol)
            {
                state = WeaponState.Rifle;
                primaryGunModel.gameObject.SetActive(true);
                pistolGunModel.gameObject.SetActive(false);
                animator.SetBool("RifleEquiped", true);
                animator.SetBool("PistolEquiped", false);
            }
        }

        if (inputManager.pistolGun)
        {
            if (state == WeaponState.Unarmed)
            {
                state = WeaponState.Pistol;
                pistolGunModel.gameObject.SetActive(true);
                animator.SetBool("PistolEquiped", true);
            }
            else if (state == WeaponState.Rifle)
            {
                state = WeaponState.Pistol;
                pistolGunModel.gameObject.SetActive(true);
                primaryGunModel.gameObject.SetActive(false);
                animator.SetBool("PistolEquiped", true);
                animator.SetBool("RifleEquiped", false);
            }
        }

        if (inputManager.putAwayWeapon)
        {
            state = WeaponState.Unarmed;
            animator.SetBool("PistolEquiped", false);
            animator.SetBool("RifleEquiped", false);
        }

        if (state != WeaponState.Unarmed)
        {
            playerController.DisablePlayerSprinting(true);
        }
        else
        {
            playerController.DisablePlayerSprinting(false);
            primaryGunModel.gameObject.SetActive(false);
            pistolGunModel.gameObject.SetActive(false);
        }

        if(inputManager.aim && state != WeaponState.Unarmed)
        {
            animator.SetBool("Aiming", true);
            playerController.SetRotationMode(EasyCharacterMovement.RotationMode.OrientToCameraViewDirection);
        }
        else
        {
            animator.SetBool("Aiming", false);
            playerController.SetRotationMode(EasyCharacterMovement.RotationMode.OrientToMovement);
        }

        
    }
}
