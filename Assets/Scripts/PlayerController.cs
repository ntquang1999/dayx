using EasyCharacterMovement;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Animations.Rigging;

public class PlayerController : Character
{
    private RigBuilder rigBuilder;

    private float animatorSpeedBlend;

    private float speedXBlend;
    private float speedYBlend;

    private InputManager inputManager;

    //Temp fields

    private bool rifleEquiped;

    private bool pistolEquiped;

    protected override void OnStart()
    {
        base.OnStart();

        Cursor.lockState = CursorLockMode.Locked;
        inputManager = FindObjectOfType<InputManager>();
        rigBuilder = GetComponentInChildren<RigBuilder>();
    }

    protected override void Animate()
    {
        //Not calling base because base is empty by default
        animatorSpeedBlend = Mathf.Lerp(animatorSpeedBlend, GetSpeed(), 10f * Time.deltaTime);
        animator.SetFloat("Speed", animatorSpeedBlend);

        animator.SetBool("Grounded", IsGrounded());

        //animator.SetBool("Aiming", (rifleEquiped || pistolEquiped)  && inputManager.aim);

        if (GetRotationMode() == RotationMode.OrientToCameraViewDirection)
        {
            float targetX;
            if (Mathf.Abs(inputManager.move.x) > 0)
                targetX = GetSpeed() * inputManager.move.x;
            else
                targetX = 0f;

            speedXBlend = Mathf.Lerp(speedXBlend, targetX, deltaTime * 10);
            animator.SetFloat("DirectionX", speedXBlend);

            float targetY;
            if (Mathf.Abs(inputManager.move.y) > 0)
                targetY = GetSpeed() * inputManager.move.y;
            else
                targetY = 0f;

            speedYBlend = Mathf.Lerp(speedYBlend, targetY, deltaTime * 10);
            animator.SetFloat("DirectionZ", speedYBlend);
        }
    }

    protected override void OnJumped()
    {
        base.OnJumped();
        animator.SetTrigger("Jump");
        print("Called");
    }

    /// <summary>
    /// True to Disable sprinting!!!
    /// </summary>
    /// <param name="value"></param>
    public void DisablePlayerSprinting(bool value)
    {
        canEverSprint = !value;
    }
}
