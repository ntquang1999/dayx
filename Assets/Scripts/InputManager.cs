using UnityEngine;
using UnityEngine.InputSystem;


public class InputManager : MonoBehaviour
{
	[Header("Character Input Values")]
	public Vector2 move;
	public Vector2 look;
	public bool jump;
	public bool sprint;
	public bool aim;
	public bool fire;
	public bool primaryGun;
	public bool pistolGun;
	public bool putAwayWeapon;
	public bool forceShowCursor = false;

	[Header("Movement Settings")]
	public bool analogMovement;

	[Header("Mouse Cursor Settings")]
	public bool cursorLocked = true;
	public bool cursorInputForLook = true;

	public void OnMove(InputValue value)
	{
		move = value.Get<Vector2>();
	}

	public void OnLook(InputValue value)
	{
		if (cursorInputForLook)
		{
			look = value.Get<Vector2>();
		}
	}

	public void OnJump(InputValue value)
	{
		jump = value.isPressed;
	}

	public void OnSprint(InputValue value)
	{
        sprint = value.isPressed;
    }

	public void OnAim(InputValue value)
	{
        aim = value.isPressed;
    }

	public void OnFire(InputValue value)
	{
		fire = value.isPressed;
	}

	public void OnPrimaryGun(InputValue value)
	{
		primaryGun = value.isPressed;
	}

	public void OnPistolGun(InputValue value)
	{
		pistolGun = value.isPressed;
	}

	public void OnPutAwayWeapon(InputValue value)
	{
		putAwayWeapon = value.isPressed;
	}

	public void OnSwitchCusor(InputValue value)
	{
		SwitchCusorState();
	}

	public void SwitchCusorState()
	{
		if (forceShowCursor)
		{
			Cursor.lockState = CursorLockMode.None;
			return;
		}

		if (cursorLocked)
			Cursor.lockState = CursorLockMode.None;
		else
			Cursor.lockState = CursorLockMode.Locked;
		cursorLocked = !cursorLocked;
	}

	public void SetCursorState(bool newState)
	{
		Cursor.lockState = newState ? CursorLockMode.Locked : CursorLockMode.None;
	}

    private void LateUpdate()
    {
		//Clean up button flags
		//Button: Press once, can't be in hold state
		//Note: Input must be processed in Update(Before lateupdate)
		jump = false;
		primaryGun = false;
		pistolGun = false;
		putAwayWeapon = false;
    }
}